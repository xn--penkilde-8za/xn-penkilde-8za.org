# åpenkilde.org

This is the homepage for [ÅPENKILDE](https://www.åpenkilde.org).

It is currently a static homepage, which was intended to run on [khttpsd](https://gitlab.com/xn--penkilde-8za/khttpsd), but due to time constraints [Drogon](https://github.com/drogonframework/drogon) is currently used.

Drogon is a general-purpose C++-based HTTP application framework. Drogon supports Linux, macOS, FreeBSD, OpenBSD, HaikuOS, and Windows. It is currently the [fastest http server](https://www.techempower.com/benchmarks/) in the world.

åpenkilde.org will switch our own general-purpose HTTP application framework, khttpsd, to outperform Drogon in static file hosting.

## System Requirements

* The Linux kernel should be not lower than 2.6.9, 64-bit version;
* The gcc version is not less than 5.4.0;
* Use cmake as the build tool, and the cmake version should be not less than 3.5;
* Use git as the version management tool;

## Library Dependencies

* trantor, a non-blocking I/O C++ network library, also developed by the author of Drogon, has been used as a git repository submodule, no need to install in advance;
* jsoncpp, JSON's c++ library, the version should be **no less than 1.7**;
* libuuid, generating c library of uuid;
* zlib, used to support compressed transmission;
* OpenSSL, not mandatory, if the OpenSSL library is installed, drogon will support HTTPS as well, otherwise drogon only supports HTTP.
* libbrotli, not mandatory, if the libbrotli library is installed, drogon will support brotli compression when sending HTTP responses;

## Installation

Assuming that the above environment and library dependencies are all ready, the installation process is very simple;

#### Install on Linux

```shell
cd $WORK_PATH
git clone https://gitlab.com/xn--penkilde-8za/xn-penkilde-8za.org.git
cd xn-penkilde-8za.org
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make
```

The default is to compile the debug version. If you want to compile the release version, the cmake command should take the following parameters:

```shell
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_CTL=OFF -DBUILD_EXAMPLES=OFF -DBUILD_ORM=OFF  ..
```
